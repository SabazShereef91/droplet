//
//  ViewController.swift
//  Droplet
//
//  Created by sabaz shereef on 30/04/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import UIKit
import FirebaseAuth
import Toaster

class ViewController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var Password_Height: NSLayoutConstraint!
   
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var MobileNumberTxt: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
   
   
    override func viewDidLoad() {
        LoginButton.isHidden = true
        Password_Height.constant = 0
    }
   
   
   
    @IBAction func GetOtp(_ sender: Any) {
       
        guard let phone =  MobileNumberTxt.text,MobileNumberTxt.text != "" else {
            Toast(text: "Please enter the  Mobile Number").show()
            return }
       
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
           
            if error == nil
            {
                self.Password_Height.constant = 34
                self.LoginButton.isHidden = false
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                
               
            }
               
            else
            {
                
               
                Toast(text: error?.localizedDescription ?? "error").show()
               
               
            }
           
        }
       
    }
   
   
   
   
   
    @IBAction func LoginAction(_ sender: Any) {
       
        guard let password = PasswordTxt.text,MobileNumberTxt.text != "",PasswordTxt.text != "" else {
             Toast(text: "Please enter the  fields").show()
            
            return
            
        }
        
        guard let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") else {
            Toast(text: "Something Went wrong").show()
            return
            
        }
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID ,
            verificationCode: password)
       
        DispatchQueue.main.async { self.showActivityIndicator() }
        Auth.auth().signIn(with: credential) { (authResult, error) in
            DispatchQueue.main.async { self.hideActivityIndicator() }
            if error == nil{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterForm") as! RegisterForm
                newViewController.PhoneNumber = self.MobileNumberTxt.text ?? ""
                self.MobileNumberTxt.text = ""
                self.PasswordTxt.text = ""
                self.LoginButton.isHidden = true
                self.Password_Height.constant = 0

                self.present(newViewController, animated: true, completion: nil)
            }
            else {
                Toast(text: "Please enter the correct code").show()
            }
        }
    }
   
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        if string == " " {
            return false
        }
        return true
    }
   
   
   
   
   
}
