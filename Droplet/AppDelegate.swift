//
//  AppDelegate.swift
//  Droplet
//
//  Created by sabaz shereef on 30/04/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

import Toaster

//import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        configureToast()
       
       
        return true
    }

    func configureToast() {
        let toastAppear = ToastView.appearance()
        toastAppear.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastAppear.textColor = .white
        toastAppear.font = UIFont.systemFont(ofSize: 16, weight: .medium) //.boldSystemFont(ofSize: 16)
        toastAppear.textInsets = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        toastAppear.bottomOffsetPortrait = 75
    }
}

