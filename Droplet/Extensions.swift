//
//  Extensions.swift
//  Droplet
//
//  Created by Sabaz on 03/05/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import Foundation
import UIKit

//MARK:- String
extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    

    
    func isValidMobile() -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: self)
    }
    
    func isValid() -> Bool {
        if self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            
            return false
        }
        return true
    }
}



//MARK:UIViewController Extension
extension UIViewController {
    
    //Show Activity Indicator
    public var activityIndicatorTag: Int { return 999999 }
    public func showActivityIndicator(hideUI: Bool = false, _ style: UIActivityIndicatorView.Style = .whiteLarge, location: CGPoint? = nil) {
        
        if self.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).count < 1 {
            UIApplication.shared.beginIgnoringInteractionEvents()
            let loc = location ?? self.view.center
            
            DispatchQueue.main.async(execute: {
                let activityIndicator = UIActivityIndicatorView(style: style)
                activityIndicator.color = hideUI ? UIColor.clear: .black
                activityIndicator.tag = self.activityIndicatorTag
                activityIndicator.center = loc
                activityIndicator.hidesWhenStopped = true
                activityIndicator.startAnimating()
                self.view.addSubview(activityIndicator)
            })
        }
    }
    
    
    
    public func hideActivityIndicator() {
        UIApplication.shared.endIgnoringInteractionEvents()
        DispatchQueue.main.async(execute: {
            if let activityIndicator = self.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        })
    }
}
