//
//  RegisterForm.swift
//  Droplet
//
//  Created by sabaz shereef on 03/05/20.
//  Copyright © 2020 sabaz shereef. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import Firebase
import SDWebImage
import Toaster


import CryptoSwift
import Foundation
let key = "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw"
let iv = "gqLOHUioQ0QjhuvI"

let string = "hello"

let arraykey: [UInt8] = Array(key.utf8)
let arrayiv: [UInt8] = Array(iv.utf8)
var Places:String = ""
weak var LocationTextfield: UITextField!

class RegisterForm: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {
    
    @IBOutlet weak var FirstNametxt: UITextField!
    @IBOutlet weak var LastNametxt: UITextField!
    @IBOutlet weak var PhoneNumbertxt: UITextField!
    @IBOutlet weak var emailidtxt: UITextField!
    @IBOutlet weak var Locationtxt: UITextField!
    @IBOutlet weak var ProfilePicture: UIImageView!
    var imagePicker = UIImagePickerController()
    
    
    let userDetails = NSMutableDictionary()
    var Check: Bool! = true
    var imagestr = ""
    var strv = ""
    var PhoneNumber = ""
    
    let qref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        prefilWithExistingData()
    }
    
    
    @IBAction func ProfilePhotoBtn(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self // as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func SaveBtn(_ sender: Any) {
        self.view.endEditing(true)
        
        //Validation
        if let errorMsg = validateForm() {
            Toast(text: errorMsg).show()
            return
        }
        
        
        let ref = Database.database().reference().child(PhoneNumber)
        DispatchQueue.main.async { self.showActivityIndicator() }
        
        if Check {
            fbAddData(dat: userDetails, ref: ref, completion: {
                (stat, reason, respDat) -> Void in
                DispatchQueue.main.async { self.hideActivityIndicator() }
                guard stat else {
                    Toast(text: reason).show()
                    return
                }
                Toast(text: "User Details added successfully").show()
            })
            
        }
        else{
            
            fbUpdateData(dat: userDetails, ref: ref, completion: {
                (stat, reason, respDat) -> Void in
                DispatchQueue.main.async { self.hideActivityIndicator() }
                guard stat else {
                    Toast(text: reason).show()
                    return
                }
                Toast(text: "User Details updated successfully").show()
            })
        }
    }
    
    
    @IBAction func Logout(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
            DispatchQueue.main.async {
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                
                
                Toast(text: "Logged out successfully").show()
                self.dismiss(animated: true, completion: nil)
            }
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let resizedImage = image.resizeWithWidth(width: 200)
            ProfilePicture.image = resizedImage
            imagestr = convertImageToBase64String(img: resizedImage ?? image)
            userDetails.setValue( try? imagestr.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_profilePic")
            UserDefaults.standard.set(imagestr, forKey: "imagestr")
        }
    }
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    func convertBase64StringToImage (imageBase64String:String) -> UIImage? {
        if let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0)) {
            let image = UIImage(data: imageData)
            return image
        }
        return UIImage(named: "user_profil")
        
    }
    
    
}


extension RegisterForm {
    
    func validateForm() -> String? {
        

        if ProfilePicture.image == nil {
            return "Please add your Picture"
        }
        
        guard let fname = FirstNametxt.text,fname != "" else {
            return "Please add first name"
        }
        
        guard let lname = LastNametxt.text, lname != "" else {
            return "Please add last name"
        }
        
        
        guard let phnum = PhoneNumbertxt.text, phnum != "" else {
            return "Please add a phone number"
        }
        
        guard phnum.isValidMobile() else {
            return "Please add a valid phone number"
        }
        
        guard let email = emailidtxt.text, email != "" else {
            return "Please add email id"
        }
        
        guard email.isValidEmail() else {
            return "Please add a valid email id"
        }
        
        guard let loctn = Locationtxt.text, loctn != "" else {
            return "Please add your location"
        }
        
        
        
        userDetails.setValue( try? fname.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_firstName")
        userDetails.setValue( try? lname.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_lastName")
        userDetails.setValue( try? phnum.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_phoneNum")
        userDetails.setValue( try? email.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_email")
        userDetails.setValue( try? loctn.aesEncrypt(key: arraykey, iv: arrayiv), forKey: "usr_location")
        
        
        return nil
    }
}

extension RegisterForm {
    
    func prefilWithExistingData() {
        DispatchQueue.main.async { self.showActivityIndicator() }
        qref.child(PhoneNumber).observeSingleEvent(of: .value, with: { (snapshot) in
            DispatchQueue.main.async { self.hideActivityIndicator() }
            if snapshot.exists(){
                let value = snapshot.value as? NSDictionary
                
                self.FirstNametxt.text = try? ( value?["usr_firstName"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                
                self.LastNametxt.text = try? ( value?["usr_lastName"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                
                self.PhoneNumbertxt.text = try? ( value?["usr_phoneNum"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                
                self.emailidtxt.text = try? ( value?["usr_email"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                
                self.Locationtxt.text = try? ( value?["usr_location"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                
                let imageString  = try? ( value?["usr_profilePic"] as? String ?? "").aesDecrypt(key: arraykey, iv: arrayiv)
                                
                self.ProfilePicture.image = self.convertBase64StringToImage(imageBase64String: imageString ?? "")
                self.Check = false
            }
            else {
               
                return
            }
        })
    }
}


extension UIImage {
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: width)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

//MARK:- FIR DB helper methods
extension RegisterForm {
    
    func fbAddData(dat: NSDictionary, ref: DatabaseReference, completion: @escaping (_ status: Bool, _ reason: String, _ respDat: String?)-> Void) {
        ref.setValue(dat){
           (error, snpshot) in
          
            if error != nil {
                completion(false,error!.localizedDescription,nil)
            }
            else {
                completion(true,"success","")
                return
            }
        }
    }
    
    func fbUpdateData(dat: NSDictionary, ref: DatabaseReference, completion: @escaping (_ status: Bool, _ reason: String, _ respDat: String?)-> Void) {
        
        
        ref.updateChildValues(dat as! [AnyHashable : Any]) {
            (error, snpshot) in
            
            if error != nil {
                completion(false,error!.localizedDescription,nil)
            }
            else {
                completion(true,"success","")
                return
            }
        }
    }
}

extension String {
    func aesEncrypt(key: [UInt8], iv: [UInt8]) throws -> String{
        let data = self.data(using: String.Encoding.utf8)
        let enc = try! AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).encrypt([UInt8](data!))
        let encData = NSData(bytes: enc, length: Int(enc.count))
        let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let result = String(base64String)
        return result
    }
    
    func aesDecrypt(key: [UInt8], iv: [UInt8]) throws -> String {
        let data = NSData(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0)) 
        let dec = try! AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).decrypt([UInt8](data!))
        let decData = NSData(bytes: dec, length: Int(dec.count))
        let result = NSString(data: decData as Data, encoding: String.Encoding.utf8.rawValue)
        return String(result!)
    }
}
